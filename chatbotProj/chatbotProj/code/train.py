""" Using Grid Search and K Folding Cross Validation to train the model

Requirements:
- Numpy
- Pandas
- Pickle
- Sklearn
"""

import numpy
import pandas
import pickle
import os

from sklearn import metrics
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split, StratifiedKFold, KFold
from sklearn.preprocessing import LabelEncoder
from sklearn.svm import SVC
from statistics import mean
from time import time

from .preprocessing import remove_stop_words, stem_words

dataset = os.path.join(os.path.dirname(
	os.path.abspath(__file__)), "data", "csv", "QA.csv")
svm_model = os.path.join(os.path.dirname(
	os.path.abspath(__file__)), "data", "pickle", "svm_model.pickle")
tfidf_model = os.path.join(os.path.dirname(
	os.path.abspath(__file__)), "data", "pickle", "tfidf_model.pickle")
label_encoder_model = os.path.join(os.path.dirname(
	os.path.abspath(__file__)), "data", "pickle", "label_encoder_model.pickle")

def preprocessing_text(text):
	"""
	Preprocessing Text
	-------------------
	Tokenization, Stop Word Removal dan Stemming Bahasa Indonesia

	PARAMETER
	-------------------
	text : string
	stopWord_factory : memanggil fungsi StopWordRemoverFactory untuk memulai proses stop word
	stemmer_factory : memanggil fungsi StemmerFactory untuk memulai proses stemming

	RETURN
	-------------------
	text : string yang telah melewati stop word removal dan stemming sekaligus tertokenisasi
	"""

	return stem_words(remove_stop_words(text))

def openCSV(file):
	"""
	Open CSV
	-------------------
	Membuka file csv

	PARAMETER
	-------------------
	file : path ke file csv

	RETURN
	-------------------
	data : list dataset dari csv file
	feature : list features dataset dari csv file
	"""

	data = pandas.read_csv(file)
	features = data['Question'].values

	list_feature = []
	for feature in features:
		list_feature.append(preprocessing_text(feature))

	return data, list_feature

def find_best_parameter(features, labels, n_fold):
	"""
	Find Best Parameter
	-------------------
	Mencari parameter C terbaik untuk model SVM

	PARAMETER
	-------------------
	features : list features dari dataset
	labels : list labels dari dataset
	n_fold : jumlah fold

	RETURN
	-------------------
	best_parameter : parameter C terbaik dan hasil akurasinya
	"""

	parameter_value = [1, 0.1, 0.01, 0.001]
	stratified = StratifiedKFold(n_splits=n_fold)
	
	best_parameter = {'C':None, 'accuracy':0}
	max_score = 0

	print('=' * 80)
	print('Grid Seacrh & Cross Validation')
	print(stratified.n_splits, 'fold')

	for c in parameter_value:
		print('*'*40)
		print('C : ', c)

		model = SVC(kernel='linear', C=c)
		print(model)
		accuracy_kfold = []

		for (train, test) in (stratified.split(features, labels)):
			model.fit(features[train], labels[train])
			labels_predict = model.predict(features[test])
			accuracy_score = numpy.sum(labels_predict == labels[test]) / labels[test].shape[0]
			accuracy_kfold.append(accuracy_score)
			# print(accuracy_score)

		average_accuracy = mean(accuracy_kfold)
		print("Average Accuracy: {0:.3f}%".format(average_accuracy*100))


		if average_accuracy > max_score:
			max_score = average_accuracy
			best_parameter['C'] = c
			best_parameter['accuracy'] = max_score

			print("Best Parameters : ", best_parameter)

	return best_parameter

def main():
	"""
	Main
	-------------------
	Bagian utama dari training model
	"""

	label_encoder = LabelEncoder()
	# tfidf_vectorizer = TfidfVectorizer()
	# tfidf_vectorizer = TfidfVectorizer(min_df=5)
	tfidf_vectorizer = TfidfVectorizer(sublinear_tf=True, norm='l2', ngram_range=(1, 2))

	data, question = openCSV(dataset)
	
	features = tfidf_vectorizer.fit_transform(question)
	labels = label_encoder.fit_transform(data['Label'])

	features.shape

	tfidf_file = open(tfidf_model, 'wb')
	pickle.dump(tfidf_vectorizer, tfidf_file)
	tfidf_file.close()

	label_encoder_file = open(label_encoder_model, 'wb')
	pickle.dump(label_encoder, label_encoder_file)
	label_encoder_file.close()

	print('Labels : ', numpy.unique(data['Label']))
	# print('Features : ', features[:1])

	fold = 10
	best_parameter = {'C':None, 'accuracy':0}
	best_parameter = find_best_parameter(features, labels, fold)

	model = SVC(kernel='linear', C=best_parameter['C'])
	accuracy_kfold = []
	confusion_matrix_kfold = []
	stratified = StratifiedKFold(n_splits=fold)
	train_time = 0
	test_time = 0

	print('=' * 80)
	print('Training: ')
	print(stratified.n_splits, 'fold')
	print(model)

	for (train, test) in (stratified.split(features, labels)):
		train_time_start = time()
		model.fit(features[train], labels[train])
		train_duration = time() - train_time_start
		train_time = train_time + train_duration

		test_time_start = time()
		labels_predict = model.predict(features[test])
		train_duration = time() - test_time_start
		test_time = test_time + train_duration

		accuracy_score = numpy.sum(labels_predict == labels[test]) / labels[test].shape[0]
		accuracy_kfold.append(accuracy_score)

		confusion_matrix_kfold.append(confusion_matrix(labels[test], labels_predict))

	file = open(svm_model, 'wb')
	pickle.dump(model, file)
	file.close()

	confusion_matrix_all = numpy.zeros((4,4), dtype=int)

	for index in range(0,(len(confusion_matrix_kfold))):
		confusion_matrix_all = numpy.add(confusion_matrix_all, confusion_matrix_kfold[index])

	average_accuracy = mean(accuracy_kfold)

	print("Confusion Matrix: ", confusion_matrix_all)
	print("Accuracy: {0:.3f}%".format(average_accuracy*100))
	print("train time: %0.3fs" % train_time)
	print("test time: %0.3fs" % test_time)

main()