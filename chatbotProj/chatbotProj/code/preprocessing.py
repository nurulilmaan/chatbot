'''
Preprocessing Text

Reference : 
PySastrawi

'''
import os, re
from Sastrawi.Dictionary.ArrayDictionary import ArrayDictionary
from Sastrawi.Stemmer.Context.Context import Context
from Sastrawi.Stemmer.Filter import TextNormalizer
from Sastrawi.Stemmer.Context.Visitor.VisitorProvider import VisitorProvider
from .stopwords import stop_word_list

kata_dasar = os.path.join(os.path.dirname(
    os.path.abspath(__file__)),"kata-dasar.txt")

with open(kata_dasar, 'r') as f:
	dictionaryContent = f.read()
dictionary = ArrayDictionary(dictionaryContent.split('\n'))

def remove_plural(plural):
	"""
	Remove Plural
	-------------------
	Stemming kata plural menjadi kata dasarnya

	PARAMETER
	-------------------
	word : kata yang akan diproses

	RETURN
	-------------------
	word : kata plural yang telah distemming
	"""
	matches = re.match(r'^(.*)(.*)$', plural)
	if not matches:
		return plural
	words = [matches.group(1), matches.group(2)]
	suffix = words[1]
	suffixes = ['ku', 'mu', 'nya', 'lah', 'kah', 'tah', 'pun']
	matches = re.match(r'^(.*)(.*)$', words[0])
	if suffix in suffixes and matches:
		words[0] = matches.group(1)
		words[1] = matches.group(2) + '-' + suffix

	rootWord1 = remove_singular(words[0])
	rootWord2 = remove_singular(words[1])

	if not dictionary.contains(words[1]) and rootWord2 == words[1]:
		rootWord2 = remove_singular('me' + words[1])

	if rootWord1 == rootWord2:
		return rootWord1
	else:
		return plural

def remove_singular(word):
	"""
	Remove Singular
	-------------------
	Stemming kata singular menjadi kata dasarnya

	PARAMETER
	-------------------
	word : kata yang akan diproses

	RETURN
	-------------------
	word : kata singular yang telah distemming
	"""
	context = Context(word, dictionary, VisitorProvider())
	context.execute()
	return context.result

def stem_words(text):
	"""
	Stem Words
	-------------------
	Mengubah kata yang memiliki imbuhan menjadi kata dasar

	PARAMETER
	-------------------
	text : kalimat yang akan diproses

	RETURN
	-------------------
	text : sekumpulan kata yang telah distemming
	"""
	text = TextNormalizer.normalize_text(text)
	words = text.split(' ')
	stems = []

	for word in words:
		if word.endswith(('ku', 'mu', 'nya', 'lah', 'kah', 'tah', 'pun')):
			stems.append(remove_plural(word))
		else:
			stems.append(remove_singular(word))

	return ' '.join(stems)

def remove_stop_words(text):
	"""
	Remove Stop Words
	-------------------
	Menghapus Stop Word sekaligus tokenization

	PARAMETER
	-------------------
	text : kalimat yang akan diproses

	RETURN
	-------------------
	text : sekumpulan kata yang telah ditokenisasi dan dihapus stop wordnya
	"""

	words = text.split(' ')
	for word_check in stop_word_list:
		for word in words:
			if word == word_check:
				words.remove(word)

	return ' '.join(words)