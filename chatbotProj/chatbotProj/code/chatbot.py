""" Reply process for the chatbot

Requirements:
- Numpy
- Pickle
- Sklearn
"""

# from .cosine import cosine_similarity
from .default_text import GREETING_INPUTS, GREETING_RESPONSES, BYE_INPUTS, BYE_RESPONSES, TY_INPUT, TY_RESPONSES, default
from .preprocessing import remove_stop_words, stem_words

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics.pairwise import cosine_similarity

import numpy
import os
import pandas
import pickle
import random

dataset = os.path.join(os.path.dirname(
	os.path.abspath(__file__)), "data", "csv", "QA.csv")
svm_model = os.path.join(os.path.dirname(
	os.path.abspath(__file__)), "data", "pickle", "svm_model.pickle")
tfidf_model = os.path.join(os.path.dirname(
	os.path.abspath(__file__)), "data", "pickle", "tfidf_model.pickle")
label_encoder_model = os.path.join(os.path.dirname(
	os.path.abspath(__file__)), "data", "pickle", "label_encoder_model.pickle")

class ChatBot(object):
	def __init__(self):
		self.score = 0.5
		self.tfidf_vectorizer = self.loadPickle(tfidf_model)
		self.label_encoder = self.loadPickle(label_encoder_model)
		self.model = self.loadPickle(svm_model)
		self.data = pandas.read_csv(dataset)
		self.questions = self.data['Question'].values

	def loadPickle(self, file):
		if os.path.exists(file):
			with open(file, 'rb') as f:
				model = pickle.load(f)
			return model
		else:
			return None

	def preprocessing_text(self, text):
		return stem_words(remove_stop_words(text.lower()))

	def get_top5(indexes):
		index_array = []

		for (index, element) in enumerate(indexes):
			index_array.append((element, index))
		index_array.sort()

		index_top5 = []
		for index in index_array[-5:]:
			index_top5.append(index[1])

		return index_top5[::-1]

	def check_prebuilt(self, text):
		answer = {"Question" : text, "Label" : "Greeting", "Answer" : None, "Score": 0, "Link" : None}

		if not text:
			answer["Answer"] = "Maaf saya tidak mengerti"
			return answer

		for each_greeting in GREETING_INPUTS:
			if text.lower() in each_greeting.lower():
				ind = GREETING_INPUTS.index(each_greeting)
				answer["Answer"] = GREETING_RESPONSES[ind]
				return answer

		for each_bye in BYE_INPUTS:
			if text.lower() in each_bye.lower():
				ind = BYE_INPUTS.index(each_bye)
				answer["Answer"] = BYE_RESPONSES[ind]
				return answer

		for each_thank in TY_INPUT:
			if text.lower() in each_thank.lower():
				ind = TY_INPUT.index(each_thank)
				answer["Answer"] = TY_RESPONSES[ind]
				return answer

		print("Not Prebuilt")
		return None

	def find_similar(self, word):
		answer = {"Question" : word, "Label" : "Greeting", "Answer" : None, "Score": 0, "Link" : None}
		self.tfidf_message = numpy.asarray(self.tfidf_vectorizer.transform([self.preprocessing_text(word)]).todense())
		self.label_message = self.label_encoder.inverse_transform(self.model.predict(self.tfidf_message))[0]
		self.questionset = self.data[self.data['Label']==self.label_message]
		print("Kamu bertanya tentang {}".format(self.label_message))

		cosine_score = []
		for question in self.questionset["Question"]:
			self.tfidf_question = numpy.asarray(self.tfidf_vectorizer.transform([self.preprocessing_text(question)]).todense())
			similarity = cosine_similarity(self.tfidf_message, self.tfidf_question)
			cosine_score.append(similarity)

		self.highest_score = max(cosine_score)
		index = cosine_score.index(self.highest_score)

		answer["Question"] = self.questionset["Question"][self.questionset.index[index]]
		answer["Label"] = self.label_message
		answer["Answer"] = self.questionset["Answer"][self.questionset.index[index]]
		answer["Score"] = self.highest_score

		if len(cosine_score) > 1 or answer['Score'] < self.score:
			if answer['Score'] < self.score:
				answer["Label"] = "Default"
				answer["Answer"] =  "Mungkin maksud anda \"" + answer["Question"] + "\"? " + default[random.randint(0,len(default)-1)]
				answer["Link"] = "https://www.google.com/search?q=%s" % ("+".join(word.split(" ")))

			return answer

		return answer

	def answer_question(self, answer, text):
		if answer['Score'] < self.score:
			answer["Label"] = "Default"
			answer["Answer"] =  default[random.randint(0,len(default)-1)]		
			answer["Link"] = "https://www.google.com/search?q=%s" % ("+".join(text.split(" ")))

		return answer

	def getReply(self, text):
		answer = self.check_prebuilt(text)

		if answer is None:
			answer = self.find_similar(text)

		return answer
