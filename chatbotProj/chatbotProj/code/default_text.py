GREETING_INPUTS = (
	"assalamualaikum",
	"hai",
	"hay",
	"hello",
	"hey",
	"hi",
	"hey bot",
	"hey",
	"halo",
	"selamat pagi",
	"selamat siang",
	"selamat sore",
	"selamat malam",
	"pagi",
	"halo bot",
	"pagi bot",
)

GREETING_RESPONSES = [
	"waalaikumsalam",
	"hai juga",
	"hey",
	"hi",
	"hi there",
	"hello",
	"Ada yang bisa dibantu?",
	"oh, hi!",
	"Halo juga!",
	"selamat pagi juga!",
	"selamat siang juga!",
	"selamat sore juga!",
	"selamat malam juga!",
	"pagi juga!",
	"hai juga",
	"pagi juga kamu...",
]

BYE_INPUTS = (
	"dadah",
	"dah",
	"sampai Jumpa",
	"sampai bertemu kembali",
	"selamat tinggal",
	"see you later",
	"c u",
	"bye",
	"oke bye",
	"wassalamualaikum",
	"selamat malam",
	"sampai bertemu lagi",
	"sampai berjumpa lagi",
	"sampai bertemu kembali",
)

BYE_RESPONSES = [
	"Dah",
	"Dadah",
	"Sampai jumpa juga",
	":)",
	"bye",
	"have a nice day",
	"ok!",
	"bye bye",
	"Don't forget me",
	"waalaikumsalam",
	"Selamat malam juga!",
	"iyaa!",
	"take care",
	"bye!",
]

TY_INPUT = (
	"makasih",
	"terimakasih",
	"oke ty",
	"thank you",
	"thankyou",
	"oke makasih bot",
	"oke terimakasih bot",
	"terimakasih bot",
	"oke makasih"
	)

TY_RESPONSES = [
	"Sama-sama",
	"Kembali kasih",
	"OK!",
	"You're welcome",
	"Glad to help you!",
	"iya sama sama :)",
	"Sama sama :)",
	"sama sama!",
	"iya, senang bisa membantu",
]

default = [
	"maaf, saya tidak mengerti. silahkan gunakan link ini untuk pencarian lebih lanjut ",
	"pertanyaan kamu tidak ada di kamus saya. silahkan gunakan link ini untuk pencarian lebih lanjut ",
	"bisa ulangi lebih jelas? atau gunakan link ini untuk pencarian lebih lanjut ",
	"bisa lebih detail lagi? atau gunakan link ini untuk pencarian lebih lanjut ",
	"saya tidak paham. silahkan gunakan link ini untuk pencarian lebih lanjut ",
	"maksudnya apa ya? atau gunakan link ini untuk pencarian lebih lanjut ",
]