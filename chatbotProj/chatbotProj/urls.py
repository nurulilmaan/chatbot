from django.contrib import admin
from django.urls import path
from django.conf.urls import url
from django.views.generic import RedirectView
from . import views

urlpatterns = [
	url(r'^$', views.load_chatbot_page, name='main'),
	url(r'^chat/$', views.get_chatbot_reply, name='chat'),
	url(r'^admin/', admin.site.urls, name='admin'),
	url(r'^favicon\.ico$',RedirectView.as_view(url='/static/images/favicon.ico')),
]
