import json
import os
import pickle
import requests

from django.conf import settings
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.views.generic.base import TemplateView
from django.views.generic import View
from django.views.decorators.http import require_http_methods
from django.views.decorators.csrf import csrf_exempt

from .code.chatbot import ChatBot
from .code.train import main as TrainModels

dataset = os.path.join(os.path.dirname(
	os.path.abspath(__file__)), "data", "csv", "QA.csv")
svm_model = os.path.join(os.path.dirname(
	os.path.abspath(__file__)), "data", "pickle", "svm_model.pickle")
tfidf_model = os.path.join(os.path.dirname(
	os.path.abspath(__file__)), "data", "pickle", "tfidf_model.pickle")
label_encoder_model = os.path.join(os.path.dirname(
	os.path.abspath(__file__)), "data", "pickle", "label_encoder_model.pickle")

def load_chatbot_page(request):
	if os.path.exists(svm_model) and os.path.exists(tfidf_model) and os.path.exists(label_encoder_model):
		print("File Exists")
		return render(request, 'index.html')
	else:
		print("File Does Not Exists, Training Starts Now")
		TrainModels()
		return render(request, 'index.html')

@csrf_exempt
def get_chatbot_reply(request):
	bot = ChatBot()
	if request.method == "POST":
		user_message = request.POST.get('text', None)
		chatbot_reply = bot.getReply(user_message)
		print(chatbot_reply)
		return HttpResponse(json.dumps(
			{'jawaban': chatbot_reply["Answer"], 'link': chatbot_reply["Link"]}))
	return render(request)